To demonstrate the program, it is enough, while at the root of the project, at the command line, enter the line with the parameters:

gradlew run -info -PappArgs=".\src\main\resources\in.txt .\src\main\resources\out.txt"

How it works:
The input class takes two String Path (input & output), the parameters are passed to the service method, where the content is read and indexes are analyzed and assigned to the elements, after which the outgoing file will be overwritten with the output content.

A linear variant is proposed, in one pass the elements are mapped and assembled.
The quality of the metric leaves much to be desired but covers the described case and in my opinion is very elegant:
 A subtlet is allocated in each of the words of one employee's full name, as noted after analyzing the input data - it's better to start.
 Further, for each substring from employee A, the occurrence of a substring in the employee's full name is searched.
 Thus, with the complete coincidence of the dates of birth, and the coincidence of at least one occurrence of the subword, we make the assumption
 that both elements belong to the same employee.

As an extension, it is possible to introduce an association of words (combinations of symbols for signifying one and the same sound in the name (surname), for the universal definition of the equivalence of a word, and the definition of all possible endings for Russian surnames for a more specific comparison by last name.