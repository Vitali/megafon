package vito.constants;

/**
 * @author Vitaliy Ippolitov
 */
public class Constant {
    public static String SERVER_ERR_RU = "Ошибка сервера.\n";
    public static String BINDING_ERR_RU = "Ошибка при считывании параметров.\n";
}
