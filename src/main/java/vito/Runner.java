package vito;

import static com.google.common.base.Preconditions.checkArgument;

import vito.services.IndexEmployeeServiceImpl;

/**
 * @author Vitaliy Ippolitov
 */
public class Runner {
    /*
    * @param args receives the input array of strings
    */
    public static void main(String[] args) {
        checkArgument(args.length > 1, "file path parameters expect (0, 1)");
        new IndexEmployeeServiceImpl().determineIndexes(args[0], args[1]);
    }
}