package vito.services;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import vito.constants.Constant;
import vito.models.Employee;

/**
 * @author Vitaliy Ippolitov
 */
public class IndexEmployeeServiceImpl implements IndexEmployeeService, Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexEmployeeServiceImpl.class);
    private static final java.lang.String master = "local[*]";
    private static final SparkConf conf = new SparkConf()
        .setAppName(IndexEmployeeService.class.getName())
        .setMaster(master);
    private static final JavaSparkContext context = new JavaSparkContext(conf);

    private final static Map<Employee, String> employeeMap = new ConcurrentHashMap<>();

    public java.lang.String determineIndexes(String inputFilePath, String outputFilePath) {
        if (inputFilePath == null || inputFilePath.isEmpty() ||
            outputFilePath == null || outputFilePath.isEmpty()) {
            LOGGER.error(Constant.BINDING_ERR_RU);
            return Constant.BINDING_ERR_RU;
        }

        try {
            System.setOut(new PrintStream(outputFilePath));
        } catch (Exception e) {
            LOGGER.error(Constant.BINDING_ERR_RU, e);
            return Constant.BINDING_ERR_RU;
        }

        try {
            context.textFile(inputFilePath)
                .map(IndexEmployeeServiceImpl::lineToTuple)  // here we have duplicates items
                .map(employee -> fillMapAndUnionSameEmployees(employeeMap, employee))
                .foreach(emp -> {
                    final String msg = String.format(
                        "%s|%s|%s", employeeMap.get(emp), emp.getFio(), emp.getDateOfBorn()
                    );
                    LOGGER.info(msg);
                    System.out.println(msg);
//                    Files.write(outputPath, msg.getBytes());
                });
            return "";
        } catch (Exception e) {
            LOGGER.error(Constant.SERVER_ERR_RU, e);
            return Constant.SERVER_ERR_RU;
        }
    }

    private static Employee fillMapAndUnionSameEmployees(
        Map<Employee, String> employeeIndexMap, Employee currentEmployee
    ) {
        final Employee similarEmployee = findSimilarEmployee(employeeIndexMap, currentEmployee);
        if (similarEmployee != null) {
            final String indexSimilarEmployee = employeeIndexMap.get(similarEmployee);
            employeeIndexMap.put(currentEmployee, indexSimilarEmployee);
        } else {
            final String generatedIndex = determineIndex(currentEmployee);
            employeeIndexMap.put(currentEmployee, generatedIndex);
        }
        return currentEmployee;
    }

    private static Employee findSimilarEmployee(
        Map<Employee, String> employeeIndexMap,
        Employee employee
    ) {
        final Optional<Employee> similarEmployee = employeeIndexMap.keySet().stream()
            .filter(emp -> isEmployeesSimilar(emp, employee))
            .findFirst();

        return similarEmployee.orElse(null);
    }

    private static boolean isEmployeesSimilar(Employee empOne, Employee empTwo) {
        final String employeeInMapFio = empOne.getFio();
        final String employeeOnFlyFio = empTwo.getFio();
        return !(employeeInMapFio == null || employeeOnFlyFio == null) &&
            isFiosSimilar(employeeInMapFio, employeeOnFlyFio) &&
            empOne.getDateOfBorn().equals(empTwo.getDateOfBorn());
    }

    /*
     * Determines whether two fio belong to the same employee
     * It follows the principle of selecting the subword in each word
     * and their occurrences in the compared value
     *
     */
    public static boolean isFiosSimilar(String fioOne, String fioTwo) {
        final String[] words = fioOne.split(" ");
        for (String word : words) {
            if (word.length() > 4) {
                final String prefix = word.substring(0, 4);
                if (fioTwo.toLowerCase().contains(prefix.toLowerCase())) {
                    return true;
                }
            }
        }
        return false;
    }

    private static Employee lineToTuple(String line) {
        try {
            final java.lang.String[] wordsInRow = line.split("\\|");
            final Employee employee = new Employee();
            employee.setFio(wordsInRow[0]);
            employee.setDateOfBorn(normalizeDate(wordsInRow[1]));

            return employee;
        } catch (Exception e) {
            LOGGER.error(Constant.SERVER_ERR_RU, e);
            return null;
        }
    }

    private static String determineIndex(Employee emp) {
        return emp.getDateOfBorn().substring(0, 3) + UUID.randomUUID().toString().substring(0, 4);
    }

    private static String normalizeDate(String date) {
        final java.lang.String[] numbersInDate = date.split("\\.");
        String month = numbersInDate[1];
        if (month.length() < 2) {
            month = "0" + month;
        }
        String day = numbersInDate[1];
        if (day.length() < 2) {
            day = "0" + day;
        }
        return numbersInDate[0] + "." + month + "." + day;
    }
}