package vito.services;

/**
 * @author Vitaliy Ippolitov
 */
public interface IndexEmployeeService {

    /**
     * Gets a list of names and dates
     * Delivers indexes (also ones at same Employee) and writes a list of employees to the
     * outgoing file
     * @param inputFilePath - input text file String path
     * @param outputFilePath - output text file String path
     * @return empty String if success, error message else
     */
    String determineIndexes(String inputFilePath, String outputFilePath);
}
