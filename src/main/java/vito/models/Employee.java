package vito.models;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * @author Vitaliy Ippolitov
 */
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Data
public class Employee implements Serializable {
    private java.lang.String fio;
    private java.lang.String dateOfBorn;

    @Override
    public java.lang.String toString() {
        return fio + "|" + dateOfBorn;
    }
}
