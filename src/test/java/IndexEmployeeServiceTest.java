import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Test;

import vito.services.IndexEmployeeServiceImpl;

public class IndexEmployeeServiceTest {
    @Test
    public void mainTest() throws URISyntaxException {
        final String inputFilePath = getClass().getResource("in.txt").toURI().toString();
        final String outputFilePath = getClass().getResource("out.txt").getPath();

        new IndexEmployeeServiceImpl().determineIndexes(inputFilePath, outputFilePath);
    }

    @Test
    public void similarFioTest() {
        boolean result = IndexEmployeeServiceImpl.isFiosSimilar(
            "Yana A. Petrova", "Nikol Rozmarine"
        );
        Assert.assertFalse(result);

        result = IndexEmployeeServiceImpl.isFiosSimilar(
            "Yana A. Petrova", "Jana Alex. Petrovva"
        );
        Assert.assertTrue(result);

        result = IndexEmployeeServiceImpl.isFiosSimilar(
            "Ksenija, Ivanofa", "Ivanova, Xeniya Pavlovna"
        );
        Assert.assertTrue(result);
    }
}